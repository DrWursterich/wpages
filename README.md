# wpages

An easy to use tool to observe web-pages.

## Installation

At first this repository needs to be cloned onto your machine.
```bash
git clone https://gitlab.com/DrWursterich/wpages.git
```

Go into the new clone and install [puppeteer](https://github.com/GoogleChrome/puppeteer).
```bash
npm install
```

Create a `pages` file under `/etc/wpages/`, in which you define all URLs of the pages you want to observe (one per line).

## Getting started

Just execute the `wpages` file, whenever you would like to check for updates.
Any changes, that have been made to the pages since your last execution will be printed to the command line.

__For further Information execute `wpages -h`__

