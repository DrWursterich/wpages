const puppeteer = require('puppeteer');

const args = process.argv;
if (!args || args.length <= 2) {
	console.log("no url specified");
	process.exit(1);
}
const url = process.argv[2];

(async (url) => {
	const browser = await puppeteer.launch({
		args: [
			'--no-sandbox',
			'--disable-setuid-sandbox',
			'--disable-features=site-per-process'
		]
	});
	const page = await browser.newPage();
	try {
		await page.goto(
				url,
				{
					waitUntil: 'networkidle2',
					timeout: 300000
				});
		let html = await page.content();
		console.log(html);
	} catch (error) {
		console.error(error);
		return 1;
	} finally {
		await browser.close();
	}
	return 0;
})(url);

